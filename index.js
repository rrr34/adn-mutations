const express = require ('express');
const morgan = require ('morgan');  
const Adn = require('./adn');

const app = express();
const adn = new Adn();

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
    res.send("Hello to the ADN's mutations tester!");
});

app.post('/mutation', (req, res) => {

    var data = req.body.dna;

    console.log(data);

    if(!adn.validLetters(data)){
        console.log('Invalid data');
        res.status(403).send('Invalid data');
    }else{
        var result = adn.hasMutation(data);

        if (result) {
            console.log('Mutation: ' + result);
            res.status(200).send('Mutation encountered!');
        } else {
            console.log('Mutation: '+result);
            res.status(403).send('No mutation encountered');
        }
    }
})

const port = process.env.PORT || 3000;

app.listen(port, () => 
console.log(`Listening on port ${port}!!...`));