
class Adn {

    hasMutation = (data) => {

        var n = data.length;
        var data = data;
        var secNum = 0; //Indica el número de veces que ha aparecido la secuencia de 4 letras iguales

        for ( let j = 0; j < n; j++){
            if(this.findSecuence(data[j])) {///////////////////Aquí se busca la secuencia en las FILAS
                secNum = secNum + 1;
                console.log(`Hay una secuencia repetida en la fila: ${j}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
            if(this.findSecuence(data.map((value) => { return value[j] }))) {//Aquí se busca la secuencia en las COLUMNAS
                secNum = secNum + 1;
                console.log(`Hay una secuencia repetida en la columna: ${j}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
        }

        ////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        // A partir de aquí se buscará la secuencia en las diagonales que son de la siguiente forma: 
        //  "\"  (como el símbolo backslash)

        var m = n-3; //es un número auxiliar que se usa para recorrer las diagonales
        for (let c = 0; c < m; c++){ ///aquí se recorre de la diagonal central hacia las de abajo
            if (this.findDiagDuplicatesBS(data, c, 0, c)){
                secNum = secNum + 1
                console.log(`Existe una secuencia repetida en la diagonal tipo backslash en c = ${c}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
        }
        for (let j=1; j<m; j++){//aquí se recorren las diagonales de arriba
            if (this.findDiagDuplicatesBS(data, 0, j, j)){
                secNum = secNum + 1
                console.log(`Existe una secuencia repetida en la diagonal tipo backslash en j = ${j}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        // Ahora buscaremos el patrón en las diagonales de la forma: "/" (como el símbolo Slash)
        // n=x.length; //dimensión de la matriz
        // var m=n-3; //es un número auxiliar que se usa para recorrer las diagonales
        
        for (let c=0; c<m; c++){///aquí se recorre de la diagonal central hacia las de abajo
            if (this.findDiagDuplicatesSlash(data, c, n-1, c)){
                secNum = secNum + 1
                console.log(`Existe una secuencia repetida en la dia diagonal tipo slash en c = ${c}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
        }

        for (let j = n-2; j > 2; j--){//aquí se recorren las diagonales de arriba
            if (this.findDiagDuplicatesSlash(data, 0, j, n-j-1)){
                secNum = secNum + 1
                console.log(`Existe una secuencia repetida en la dia diagonal tipo slash en j = ${j}`);
                if (secNum == 2) {
                    console.log('Existe mutación genética');
                    return true
                }
            }
        }

        console.log('Total de secuencias de caracteres repetidos = ' + secNum);
        return false;
    }

    validLetters = (array) => {
        var n = array.length
        for (let j=0; j<n; j++){
            for (let i=0; i<n; i++){
                if (array[j][i] != 'A' && array[j][i] != 'T' && array[j][i] != 'C' && array[j][i] != 'G' ){
                    return false
                }
            }
        }
        return true
    }

    findSecuence = (r) => {
        for (let i=0; i<r.length-3; i++){
            if(r[i] == r[i+1] && r[i] == r[i+2] && r[i] == r[i+3]){
                return true;
            }
        }
    }

    findDiagDuplicatesBS = (x,a,b,r) => {

        let h = x.length-3-r; //este número indica cuántas posibilidades hay de que haya una secuencia de repetición en la diagonal
        for (let i=0; i<h; i++){
            // debugd1(x,a+i,b+i);
            if( x[a+i][b+i] == x[a+1+i][b+1+i] && x[a+i][b+i] == x[a+2+i][b+2+i] && x[a+i][b+i] == x[a+3+i][b+3+i] ){
                return true
            }
        }
    }

    findDiagDuplicatesSlash = (x,a,b,r) => {

        let h = x.length-3-r; //este número indica cuántas posibilidades hay de que haya una secuencia de repetición en la diagonal

        for (let i=0; i<h; i++){
            // debugd2(x,a+i,b-i);
            if( x[a+i][b-i] == x[a+1+i][b-1-i] && x[a+i][b-i] == x[a+2+i][b-2-i] && x[a+i][b-i] == x[a+3+i][b-3-i] ){
                return true
            }
        }
    }

    debugd1 = (arre,j,k) => {

        let t = arre.length - 2;
        for (let f = 0; f < t; f++){
            console.log(arre[j+f][k+f]);
        }
    }

    debugd2 = (arre,j,k) => {
        
        let p = arre.length - 2;
        for (let f=0; f < p; f++){
            console.log('j: '+j+'  k:   '+k);
            console.log(arre[j+f][k-f]);
        }
        console.log('debug ended');
    }

}

module.exports = Adn;