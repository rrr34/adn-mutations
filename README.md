<h5>Mutaciones genéticas</h5>

Es una API escrita en Node.js y Express.js que busca mutaciones genéticas en el ADN. Se buscan secuencias de 4 caracteres repetidos en una matriz de NxN tanto de forma vertical como horizontal y diagonal. Para que exista mutación debe existir al menos 2 secuencias de repeticiones.

Si la matriz de caracteres fuera de 8x8 o mayor, el código no es capaz de encontrar dos secuencias de repetición de caracteres en una misma fila, columna o diagonal por lo que se podría decir que el código estrictamente funciona para N menor a 8.

La API recibe peticiones POST con las secuencias de ADN en formato JSON. Si no existe mutación, el status de la peticióón es 403, si sí existe, el status es 200.

Para correr el código primeramente se instalan las dependencias con "npm install" y se ejecuta con "node index.js". La API se puede probar utilizando software como Postman o Insomnia para generar las peticiones HTTP con POST, la cual se debe realizar en la ruta: localhost:3000/mutation.

En caso de que exista una variable de entorno PORT definida, la aplicación se despliega en el puerto definido en esa variable.